using System.Collections;
using System.Collections.Generic;
using Samarnggoon.GameDev3.Chapter1;
using UnityEngine;

namespace Samarnggoon.GameDev3.Chapter1
{
    
}
public class ItemTypeComponent : MonoBehaviour
{
   
    [SerializeField]
     protected ItemType m_ItemType;

     public ItemType Type
     {
         get
         {
         return m_ItemType;
          }
     set
      {
          m_ItemType = value;
         
         }
     }
    
    
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
